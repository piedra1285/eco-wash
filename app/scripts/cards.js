console.log("Cargando tarjetas");
const dataCards = [
    {
        "title": "Limpieza y brillo sin usar agua/Ultra Clean",
        "url_image":"https://pbs.twimg.com/media/A-AyurTCIAAz9MY.png",
        "desc":"Tecnología  liquida eco-amigable para el cuidado de su auto, rapido y economico, se logra encerar y lavar el auto sin agua. En cualquier sitio ",
        "cta":"Show More",
        "link":"https://www.facebook.com/Eco-Wash-lavado-en-seco-A-domicilio-692412744470635"
    },
     
    {
        "title": "Cera Liquida Eco-Wash/Easy Wax",
        "url_image":"https://img.autosblogmexico.com/2019/06/04/hVoAV2LR/4-3057.jpg",
        "desc":"Formula que permite aplicar tanto en la sombra como en el sol y no deja residuosblancos.",
        "cta":"Show More",
        "link":"https://www.facebook.com/Eco-Wash-lavado-en-seco-A-domicilio-692412744470635"
    },
    
    {
        "title": "Abrillantador de Lantas/Clean & Shine",
        "url_image":"https://www.casajgomez.com.py/wp-content/uploads/2018/09/C%C3%B3mo-hacer-brillar-las-llantas-2.jpg",
        "desc":"Proporciona un brillo profundo y solido de larga duracion, cuida el caucho protegiendo contra el agrietamiento y la perdida de color.",
        "cta":"Show More",
        "link":"https://www.facebook.com/Eco-Wash-lavado-en-seco-A-domicilio-692412744470635"
    },
    
    {
        "title": "Limpieza de aros/Easy Clean",
        "url_image":"https://www.bardahl.com.mx/wp-content/uploads/2018/07/Limpieza-Rines-Auto.jpg",
        "desc":"Formula especial de alta efictividad libre de ácidos y fosfatos. Limpiador para todo tipo de aros.",
        "cta":"Show More",
        "link":"https://www.facebook.com/Eco-Wash-lavado-en-seco-A-domicilio-692412744470635"
    },
    
    {
        "title": "Limpiador de Cristales/Clear & Clean",
        "url_image":"https://www.motor.mapfre.es/media/2018/05/como_limpiar_los_cristales_del_coche_para_que_queden_perfectos-1280x720.jpg",
        "desc":"Limpiador efectivo para todo tipo de superficie de cristal, se puede utilizar en el interior y exterior del auto, produce una capa protectora.",
        "cta":"Show More",
        "link":"https://www.facebook.com/Eco-Wash-lavado-en-seco-A-domicilio-692412744470635"
    },
    
    {
        "title": "Limpieza de motor/Power Clean",
        "url_image":"https://noticias.coches.com/wp-content/uploads/2010/08/motor-sucio-limpio-650x430.jpg",
        "desc":"Potente desengrasante. Excelente capacidad de penetracion para lograr llegar hasta los sitios más  inaccesibles.",
        "cta":"Show More",
        "link":"https://www.facebook.com/Eco-Wash-lavado-en-seco-A-domicilio-692412744470635"
    },
  
    {
      "title": "Abrillantador de motor/Renovate & Pritects",
      "url_image":"https://m.media-amazon.com/images/S/aplus-media/vc/092a78db-84cf-49bf-97da-877fd2887168.__CR0,0,970,600_PT0_SX970_V1___.jpg",
      "desc":"Formulado especialmente para la preservacion del motor, cables de encendido y tuberias, repele la suciedad y humedad",
      "cta":"Show More",
      "link":"https://www.facebook.com/Eco-Wash-lavado-en-seco-A-domicilio-692412744470635"
  },
  
  {
      "title": "Limpieza de tapicería/Eco Clean",
      "url_image":"https://i.pinimg.com/originals/01/b8/d7/01b8d764e2016767a427ee5e8d47a1f6.jpg",
      "desc":"Clasica formula de limpieza profunda. Permite eliminar todo tipo de suciedad y manchas incrustadas en el tapizado.",
      "cta":"Show More",
      "link":"https://www.facebook.com/Eco-Wash-lavado-en-seco-A-domicilio-692412744470635"
  },
  
  {
      "title": "Limpieza de vinil y cuero/Clean & Renovate",
      "url_image":"https://www.actualidadmotor.com/wp-content/uploads/2018/11/limpiar-interior-coche-consejos-830x460.jpg",
      "desc":"Proporciona un brillo intenso, humecta y cuida el vinil o cuero del vehículo, al mismo tiempo protege contra el agrietamiento.",
      "cta":"Show More",
      "link":"https://www.facebook.com/Eco-Wash-lavado-en-seco-A-domicilio-692412744470635"
  },
  
  {
      "title": "Limpieza de vinil y cuero(Todo proposito)/Clean & Protects",
      "url_image":"https://t1.uc.ltmcdn.com/images/1/2/1/como_limpiar_el_salpicadero_del_coche_50121_orig.jpg",
      "desc":"Formula para limpiar, proteger y desinfectar sin producir brillo",
      "cta":"Show More",
      "link":"https://www.facebook.com/Eco-Wash-lavado-en-seco-A-domicilio-692412744470635"
  },
    ];
  
  
  (function () {
    let CARD = {
      init: function () {
        console.log('card module was loaded');
        let _self = this;
  
        //llamanos las funciones
        this.insertData(_self);
        //this.eventHandler(_self);
      },
  
      eventHandler: function (_self) {
        let arrayRefs = document.querySelectorAll('.accordion-title');
  
        for (let x = 0; x < arrayRefs.length; x++) {
          arrayRefs[x].addEventListener('click', function(event){
            console.log('event', event);
            _self.showTab(event.target);
          });
        }
      },
  
      insertData: function (_self) {
        dataCards.map(function (item, index) {
          document.querySelector('.card-list').insertAdjacentHTML('beforeend', _self.tplCardItem(item, index));
        });
      },
  
      tplCardItem: function (item, index) {
        return(`<div class='card-item' id="card-number-${index}">
        <img src="${item.url_image}"/>
        <div class="card-info">
          <p class='card-title'>${item.title}</p>
          <p class='card-desc'>${item.desc}</p>
          <a class='card-cta' target="blank" href="${item.link}">${item.cta}</a>
        </div>
      </div>`)},
    }
  
    CARD.init();
  })();