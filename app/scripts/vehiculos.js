console.log("Cargando tarjetas");
const dataCards = [
    {
        "title": "Auto Sedan",
        "url_image":"https://elsumario.com/wp-content/uploads/2017/05/el-sumario-Samsung-ya-puede-probar-su-auto-aut%C3%B3nomo-768x432.jpg",
        "desc":"",
        "cta":"Show More",
        "link":"https://www.facebook.com/Eco-Wash-lavado-en-seco-A-domicilio-692412744470635"
    },
     
    {
        "title": "Auto 4X4",
        "url_image":"https://carnovo.com/wp-content/uploads/2019/01/SEAT-Ateca.jpg",
        "desc":"",
        "cta":"Show More",
        "link":"https://www.facebook.com/Eco-Wash-lavado-en-seco-A-domicilio-692412744470635"
    },
    
    {
        "title": "Microbus",
        "url_image":"https://www.toyotacr.com/uploads/family/fbf365314299b93089fa6805009fde34e3d2ec2c.png",
        "desc":"",
        "cta":"Show More",
        "link":"https://www.facebook.com/Eco-Wash-lavado-en-seco-A-domicilio-692412744470635"
    },
    
    ];
  
  
  (function () {
    let CARD = {
      init: function () {
        console.log('card module was loaded');
        let _self = this;
  
        //llamanos las funciones
        this.insertData(_self);
        //this.eventHandler(_self);
      },
  
      eventHandler: function (_self) {
        let arrayRefs = document.querySelectorAll('.accordion-title');
  
        for (let x = 0; x < arrayRefs.length; x++) {
          arrayRefs[x].addEventListener('click', function(event){
            console.log('event', event);
            _self.showTab(event.target);
          });
        }
      },
  
      insertData: function (_self) {
        dataCards.map(function (item, index) {
          document.querySelector('.card-list2').insertAdjacentHTML('beforeend', _self.tplCardItem(item, index));
        });
      },
  
      tplCardItem: function (item, index) {
        return(`<div class='card-item' id="card-number-${index}">
        <img src="${item.url_image}"/>
        <div class="card-info">
          <p class='card-title'>${item.title}</p>
          <p class='card-desc'>${item.desc}</p>
          <a class='card-cta' target="blank" href="${item.link}">${item.cta}</a>
        </div>
      </div>`)},
    }
  
    CARD.init();
  })();