const dataAccordion = [{
    "title":"¿Que es Eco-Wash?",
    "desc":"Empresa eco-amigable para el lavado de vehículos sin usar agua."
    },
    {
    "title": "¿Nuestro objetivo?",
    "desc": "Hacer conciencia ambiental y contribuir con el no desperdicio irracional de este líquido tan vital para la vida"      
    },
    {
    "title": "¿Funcionaietno del producto?",
    "desc": "Formulado para que al caer en la carrocería se genere una reacción química que haga desprender la suciedad y esta sea encapsulada en el líquido, para luego ser retirada con una toalla de microfibra sin ejercer fricción."
    }];
    
    
    
    (function () {
        let ACCORDION = {
            init: function () {
                let _self = this;
                //LLamamos la funciones
                this.insertData(_self);
                this.eventHandler(_self);
        },
        
        eventHandler: function (_self) {
            let arrayRefs = document.querySelectorAll('.accordion-title');
    
            for (let x = 0; x < arrayRefs.length; x++) {
                arrayRefs[x].addEventListener('click', function(event){
                    console.log('event', event);
                    _self.showTab(event.target);
                });
            }
        },
    
        showTab: function(refItem){
            let activeTab = document.querySelector('.tab-active');
    
            if (activeTab){
                activeTab.classList.remove('tab-active');
            }
    
            console.log('show tab', refItem);
            refItem.parentElement.classList.toggle('tab-active');
        },
    
        insertData: function (_self){
            dataAccordion.map(function(item, index) {
                //console.log('item!!!!', item);
                document.querySelector('.main-accordion-container').insertAdjacentHTML('beforeend', _self.tplAccordionItem(item));
            });
        },
    
       tplAccordionItem: function (item){
           return(`<div class='accordion-item'>
               <p class='accordion-title'>${item.title}</p>
               <p class='accordion-desc'>${item.desc}</p> 
               </div>`)},
       }
    
    ACCORDION.init();
    
    })();